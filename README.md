We are a full-service, board qualified, podiatric provider using both time tested practices and the latest innovations in medicine to guide how we care for our patients and manage our clinic. We treat everything from every day aches and pains to major foot and ankle injuries and deformities.

Address: 955 N McQueen Rd, Suite 1, Chandler, AZ 85225, USA

Phone: 480-744-6234

Website: http://thefootdocaz.com/
